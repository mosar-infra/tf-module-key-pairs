# computing/main.tf

resource "aws_key_pair" "ssh_key" {
  for_each   = var.keys
  key_name   = each.value.key_name
  public_key = file(each.value.public_key_path)
  tags = {
    Environment = var.environment
    ManagedBy   = var.managed_by
  }
}
