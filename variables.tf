# computing/variables.tf

variable "keys" {}
variable "environment" {}
variable "managed_by" {
  default = "key-pairs"
}
